This repository contains the analysis plan for the analysis of clinical predictors within the AIDA study.

The analysis plan describes the analysis implemented and subsequently written up in the manuscript “Low predictive power of clinical features for relapse prediction after antidepressant discontinuation in a naturalistic setting”, which has been submitted for publication.

The PDF Analysis_Plan_CP_cleanVersion.pdf contains a clean version of the final analysis plan used in the publication.
Authors: Isabel M. Berwian, Julia G. Wenzel, Leonie Kuehn, Inga Schnuerer, Erich Seifritz, Klaas E. Stephan, Henrik Walter and Quentin J. M. Huys
